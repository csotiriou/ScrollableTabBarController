//
//  ScrollableTabBarController.swift
//  ScrollableTabBarcontroller
//
//  Created by Christos Sotiriou on 02/09/16.
//  Copyright © 2016 Christos Sotiriou. All rights reserved.
//

import UIKit
import SnapKit

public struct ScrollableTabBarOptions {
    public var tabItemSize : CGSize = CGSizeMake(100, 90)
    public var tabItemHorizontalSpacing : CGFloat = 10.0
    public var tabItemVerticalInset : CGFloat = 10.0
    public var movingTabBarAlsoChangesController = false
    public var tabBarBackgroundColor = UIColor.blackColor()
}

public enum RegisterCell {
    case FromNib(nib : UINib, identifier : String)
    case FromClass(c : UICollectionViewCell.Type, identifier : String)
    
    internal func instantiate(collectionView : UICollectionView) {
        switch self {
        case .FromNib(let n, let i):
            collectionView.registerNib(n, forCellWithReuseIdentifier: i)
            break
        case .FromClass(let c, let i):
            collectionView.registerClass(c, forCellWithReuseIdentifier: i)
            break
        }
    }
}

class CollectionViewLayout : UICollectionViewFlowLayout {
    
    init(options : ScrollableTabBarOptions){
        super.init()
        
        self.itemSize = options.tabItemSize
        self.minimumLineSpacing = options.tabItemHorizontalSpacing
        self.minimumInteritemSpacing = options.tabItemHorizontalSpacing
        self.sectionInset = UIEdgeInsetsMake(0, UIScreen.mainScreen().bounds.size.width / 2 - options.tabItemSize.width / 2, 0, 0)
        
        self.invalidateLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func targetContentOffsetForProposedContentOffset(proposedContentOffset: CGPoint) -> CGPoint {
        return super.targetContentOffsetForProposedContentOffset(proposedContentOffset)
    }
    
    override func targetContentOffsetForProposedContentOffset(proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        return super.targetContentOffsetForProposedContentOffset(proposedContentOffset, withScrollingVelocity: velocity)
    }
}

public class ScrollableTab: UIView {
    private var collectionView : UICollectionView!
    
    public override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func willMoveToTabBarController(tabBarController : ScrollableTabBarController, withOptions options : ScrollableTabBarOptions){
        
        let layout = CollectionViewLayout(options: options)
        layout.scrollDirection = .Horizontal

        self.collectionView = UICollectionView(frame: self.bounds, collectionViewLayout: layout)

        self.collectionView.dataSource = tabBarController
        self.collectionView.delegate = tabBarController
        
        self.addSubview(self.collectionView)
        self.collectionView.snp_makeConstraints { (make) in
            make.edges.equalTo(self)
        }

        tabBarController.dataSource.tabBarCollectionCellsRegister().forEach({item in
            item.instantiate(self.collectionView)
        })

        self.backgroundColor = options.tabBarBackgroundColor
    }
    
    func reloadItems() -> Void {
        self.collectionView.reloadData()
    }
}

public protocol ScrollableTabBarControllerDataSource: class {
    var numberOfControllers : Int { get }
    func viewController(forIndex index : Int) -> UIViewController
    func tabBarCollectionCellsRegister() -> [RegisterCell]
    func collectionCellForTabBarItem(withIndexPath indexPath : NSIndexPath, andCollectionView collectionView : UICollectionView) -> UICollectionViewCell
    
    func options(forScrollableTabBarController controller : ScrollableTabBarController) -> ScrollableTabBarOptions
}

extension ScrollableTabBarControllerDataSource {
    func options(forScrollableTabBarController controller : ScrollableTabBarController) -> ScrollableTabBarOptions {
        return ScrollableTabBarOptions()
    }
}

@objc public protocol ScrollableTabBarControllerDelegate: class {
    optional func scrollableTabBarController(controller : ScrollableTabBarController, didTransitionToIndex index : Int, forViewController viewController : UIViewController)
}

public class ScrollableTabBarController: UIViewController {
    private var pageViewController : UIPageViewController!
    private var controllers = [UIViewController]()
    
    public weak var dataSource : ScrollableTabBarControllerDataSource!
    public weak var delegate : ScrollableTabBarControllerDelegate?
    
    public var tabBar : ScrollableTab!
    private var options : ScrollableTabBarOptions!
    
    var selectedIndex : Int {
        guard let index = self.pageViewController.viewControllers?.first!.view.tag else { return 0 }
        return index - 1
    }
}

extension ScrollableTabBarController {
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.options = self.dataSource.options(forScrollableTabBarController: self)
        self.loadControllers()
    }
    
    public override func loadView() {
        super.loadView()
    }

    private func prepareTabBar(){
        self.tabBar = ScrollableTab()
        self.view.addSubview(self.tabBar)

        self.tabBar.snp_makeConstraints { (make) in
            make.top.equalTo(self.view.snp_top)
            make.left.equalTo(self.view.snp_left)
            make.right.equalTo(self.view.snp_right)
            make.height.equalTo(100)
        }

        self.tabBar.backgroundColor = UIColor.whiteColor()
        self.tabBar.willMoveToTabBarController(self, withOptions: self.options)
    }

    private func preparePageViewController(){
        self.pageViewController = UIPageViewController(transitionStyle: .Scroll, navigationOrientation: .Horizontal, options: nil)
        self.pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.pageViewController.delegate = self
        self.pageViewController.dataSource = self

        self.addChildViewController(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)

        prefetchControllers()
        self.pageViewController.setViewControllers([self.controllers[0]], direction: .Forward, animated: false, completion: nil)

        self.pageViewController.view.snp_makeConstraints { (make) in
            make.top.equalTo(self.tabBar.snp_bottom)
            make.left.equalTo(self.view.snp_left)
            make.right.equalTo(self.view.snp_right)
            make.bottom.equalTo(self.view.snp_bottom)
        }
    }
    
    private func loadControllers(){
        self.view.translatesAutoresizingMaskIntoConstraints = false
        self.prepareTabBar()
        self.preparePageViewController()
        self.tabBar.reloadItems()
    }
    
    private func prefetchControllers() -> Void {
        for i in 1...self.dataSource.numberOfControllers {
            let currentVC = self.dataSource.viewController(forIndex: i)
            currentVC.view.tag = i
            self.controllers.append(currentVC)
        }
    }
    
    @objc //if I remove this it crashes!
    func showController(atIndex index: Int){
        let currentIndex = self.selectedIndex
        let vc = self.controllers[index]
        self.pageViewController.setViewControllers([vc], direction: index < currentIndex ? .Reverse : .Forward , animated: true, completion: nil)
    }
}


extension ScrollableTabBarController : UICollectionViewDataSource {
    public func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.controllers.count
    }
    
    public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        return self.dataSource.collectionCellForTabBarItem(withIndexPath: indexPath, andCollectionView: collectionView)
    }
}

extension ScrollableTabBarController : UICollectionViewDelegate {
    public func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        print("selected item")
        collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: .CenteredHorizontally, animated: true)
        self.showController(atIndex: indexPath.row)
    }
    
    public func collectionView(collectionView: UICollectionView, targetContentOffsetForProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        let cvBounds = collectionView.bounds
        let halfWidth = cvBounds.size.width * 0.5;
        let proposedContentOffsetCenterX = proposedContentOffset.x + halfWidth;
        
        if let attributesForVisibleCells = collectionView.collectionViewLayout.layoutAttributesForElementsInRect(cvBounds) {
            var candidateAttributes : UICollectionViewLayoutAttributes?
            
            for attributes in attributesForVisibleCells {
                if attributes.representedElementCategory != UICollectionElementCategory.Cell {
                    continue
                }
                
                if let candAttrs = candidateAttributes {
                    let a = attributes.center.x - proposedContentOffsetCenterX
                    let b = candAttrs.center.x - proposedContentOffsetCenterX
                    
                    if fabsf(Float(a)) < fabsf(Float(b)) {
                        candidateAttributes = attributes;
                    }
                }else{
                    candidateAttributes = attributes;
                    continue;
                }
            }
            
            return CGPoint(x: round(candidateAttributes!.center.x - halfWidth), y: proposedContentOffset.y)
        }
        
        return collectionView.collectionViewLayout.targetContentOffsetForProposedContentOffset(proposedContentOffset)
    }
}

extension ScrollableTabBarController : UIPageViewControllerDelegate {
    
}

extension ScrollableTabBarController : UIPageViewControllerDataSource {
    
    public func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let index = self.controllers.indexOf(viewController)!
        guard index + 1 < self.controllers.count else {return nil}
        return self.controllers[index + 1];
    }
    
    public func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let index = self.controllers.indexOf(viewController)!
        guard index > 0 else {return nil}
        return self.controllers[index - 1];
    }
    
    public func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let selectable = self.selectedIndex;
        self.tabBar.collectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: selectable, inSection: 0), atScrollPosition: .CenteredHorizontally, animated: true)
        self.delegate?.scrollableTabBarController?(self, didTransitionToIndex: selectable, forViewController: self.controllers[selectable])
    }
}


