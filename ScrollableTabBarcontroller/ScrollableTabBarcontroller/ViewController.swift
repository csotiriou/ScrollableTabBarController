//
//  ViewController.swift
//  ScrollableTabBarcontroller
//
//  Created by Christos Sotiriou on 02/09/16.
//  Copyright © 2016 Christos Sotiriou. All rights reserved.
//

import UIKit

class ttt: UICollectionViewCell {
    override var reuseIdentifier: String? {
        return "cell"
    }
}

class ViewController: ScrollableTabBarController, ScrollableTabBarControllerDataSource, ScrollableTabBarControllerDelegate {

    override func viewDidLoad() {
        self.dataSource = self
        self.delegate = self
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var numberOfControllers: Int {
        return 10
    }
    
    func title(forControllerWithIndex index: Int) -> String {
        return "title"
    }
    
    func viewController(forIndex index: Int) -> UIViewController {
        let v =  UIViewController()
        v.view.backgroundColor = UIColor.blueColor()
        let view = UIView()
        view.backgroundColor = UIColor.brownColor()
        view.frame = CGRectMake(0, 0, 100, 100)
        v.view.addSubview(view)
        return v
    }
    
    func scrollableTabBarController(controller: ScrollableTabBarController, didTransitionToIndex index: Int, forViewController viewController: UIViewController) {
        print(index)
    }
    
    func tabBarCollectionCellsRegister() -> [RegisterCell] {
        return [
            .FromClass(c: ttt.self, identifier: "cell"),
            RegisterCell.FromNib(nib: UINib(nibName: "TabStyle1CollectionViewCell", bundle: nil), identifier: "cellStyle1")
        ]
    }
    
    func collectionCellForTabBarItem(withIndexPath indexPath: NSIndexPath, andCollectionView collectionView: UICollectionView) -> UICollectionViewCell {
        let c = collectionView.dequeueReusableCellWithReuseIdentifier("cellStyle1", forIndexPath: indexPath) as! TabStyle1CollectionViewCell
        c.label.text = "Tab \(indexPath.row)"
        c.backgroundColor = UIColor.cyanColor()
        return c
    }
    
    func options(forScrollableTabBarController controller: ScrollableTabBarController) -> ScrollableTabBarOptions {
        return ScrollableTabBarOptions()
    }
}

